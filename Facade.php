<?php

namespace App;

require_once('./TP3.php');

abstract class DB
{
    public static function __callStatic(string $name, array $arguments)
    {
        // call_user_func_array([Database::getPDO(), $name], $arguments);
        // ...[1, 2, 3, 4] => 1, 2, 3, 4
        // JS
        // const a = [1, 2, 3, 4]
        // const b = [5, 6, 7, 8]
        // const c = [...a, ...b] => [1, 2, 3, 4, 5, 6, 7, 8]
        Database::getPDO()->$name(...$arguments);
    }
}

DB::prepare('SELECT * FROM table_name;', "toto", 1, null, "vert");
DB::query();
DB::lastInsertId();
