<?php

// Chain of responsibility

namespace App;

class Server
{
    /** @var [string => ['password' => string, 'role' => 'string']] */
    private array $users = [];

    public function registerUser($email, $password, $role = "ROLE_USER")
    {
        $this->users[$email] = [
            'password' => $password,
            'role' => $role
        ];
    }

    public function userExists($email): bool
    {
        return isset($this->users[$email]);
    }

    public function isPasswordValid($email, $password): bool
    {
        return $this->users[$email]['password'] === $password;
    }

    public function userHasRole($email, $role): bool
    {
        return $this->users[$email]['role'] === $role;
    }
}

abstract class Middleware
{
    protected ?Middleware $next = null;

    protected Server $server;

    public function __construct($server)
    {
        $this->server = $server;
    }

    /**
     * Permet de créer une chaîne de middlewares
     */
    public function linkWith(Middleware $next): Middleware
    {
        $this->next = $next;

        return $next;
    }

    public function handle($email, $password, $role): bool
    {
        // null == false
        // false == 0
        if (!$this->next) {
            return true;
        }

        return $this->next->handle($email, $password, $role);
    }
}

class UserExistsMiddleware extends Middleware
{
    public function handle($email, $password, $role): bool
    {
        if (!$this->server->userExists($email)) {
            echo "L'utilisateur n'éxiste pas";

            return false;
        }

        return parent::handle($email, $password, $role);
    }
}

class CheckPasswordMiddleware extends Middleware
{
    public function handle($email, $password, $role): bool
    {
        if (!$this->server->isPasswordValid($email, $password)) {
            echo "Le mot de passe fourni est erroné.";

            return false;
        }

        return parent::handle($email, $password, $role);
    }
}

class CheckRoleMiddleware extends Middleware
{
    public function handle($email, $password, $role): bool
    {
        if (!$this->server->userHasRole($email, $role)) {
            echo "Tu n'as pas les droits requis pour accéder à cette page.";

            return false;
        }

        return parent::handle($email, $password, $role);
    }
}

class Router
{
    public function handleRoute($route)
    {
        echo $route.PHP_EOL;
    }
}

function callRouter(Server $server, $email, $password, $role = "ROLE_USER")
{
    $router = new Router();

    $first = new UserExistsMiddleware($server);
    // $first
    //  ->linkWith(...)
    //  ->linkWith(...)
    //  ...;
    $first
        ->linkWith(new CheckPasswordMiddleware($server))
        ->linkWith(new CheckRoleMiddleware($server));

    if ($first->handle($email, $password, $role)) {
        $router->handleRoute('/admin');

        return;
    }

    echo "\n\n";
}

$server = new Server();
$server->registerUser('toto@admin.fr', 'toto', 'ROLE_ADMIN');
$server->registerUser('tata@truc.com', 'titi93');

callRouter($server, 'cc@test.com', 'titi', 'ROLE_USER');
callRouter($server, 'toto@admin.fr', 'unmotdepasse', "ROLE_ADMIN");
callRouter($server, 'tata@truc.com', 'titi93', "ROLE_ADMIN");
callRouter($server, 'toto@admin.fr', 'toto', "ROLE_ADMIN");

// > Faites en sorte de créer un middleware pour vérifier que le mot de passe de l'utilisateur correspond à celui enregistré
// > Créez un middleware pour vérifier que le rôle de l'utilisateur correspond à celui enregistré

// Résultat attendu
/*
UserExistsMiddleware: Le mot de passe est incorrect !
--
CheckRoleMiddleware: L'utilisateur n'a pas le role requis !
--
CheckRoleMiddleware: Hello, toto@admin.fr
Router: Successful authentication to : /admin
*/
