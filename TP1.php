<?php

// Factory Method

namespace App;

interface TransportInterface
{
    public function deliver();
}

abstract class Vehicule implements TransportInterface
{
}

class Truck extends Vehicule
{
    public function deliver()
    {
        echo "Better hit the road.\n";
    }
}

class Ship implements TransportInterface
{
    public function deliver()
    {
        echo "From sea to shining sea.\n";
    }
}

class Plane implements TransportInterface
{
    public function deliver()
    {
        echo "I believe I can fly.\n";
    }
}

/** *-----*------* */

class LogisticApp
{
    public function createDelivery(TransportInterface $deliveryMode)
    {
        $deliveryMode->deliver();
    }
}

$app = new LogisticApp();
echo "New delivery via truck:\n";
$app->createDelivery(new Truck());
echo "Now via ship:\n";
$app->createDelivery(new Ship());
$app->createDelivery(new Plane());

// Résultat attendu :
/*
New delivery via truck :
Better hit the road.
Now via ship :
From sea to shining sea.
*/
