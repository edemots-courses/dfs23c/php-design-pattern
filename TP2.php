<?php

// Abstract Factory

namespace App;

abstract class Pizza
{
    /**
     * public $from
     *
     * public function __construct($from)
     * {
     *      $this->from = $from;
     * }
     */
    public function __construct(public $from)
    {
    }

    abstract public function eat();

    abstract public function cook();

    abstract public function flip();
}

class Margherita extends Pizza
{
    public function eat()
    {
        echo 'Sei bella come una margherita napoletana!'.PHP_EOL;
    }

    public function cook()
    {
        echo 'Croccante napoletana margherita!'.PHP_EOL;
    }

    public function flip()
    {
        echo 'Un salto di margherita napoletana!'.PHP_EOL;
    }
}

class Regina extends Pizza
{
    public function eat()
    {
        echo "Mange une regina.\n";
    }

    public function cook()
    {
        echo "Cuit une regina.\n";
    }

    public function flip()
    {
        echo "Fait tourner une regina.\n";
    }
}




interface PizzaFactoryInterface
{
    public function createMargherita();

    public function createRegina();
}

class NeapolitanPizzaFactory implements PizzaFactoryInterface
{
    public function createMargherita(): Pizza
    {
        return new Margherita("Naples");
    }

    public function createRegina(): Pizza
    {
        return new Regina("Naples");
    }
}

class RomanPizzaFactory implements PizzaFactoryInterface
{
    public function createMargherita()
    {
        return new Margherita("Rome");
    }

    public function createRegina()
    {
        return new Regina("Rome");
    }
}

class VenetianPizzaFactory implements PizzaFactoryInterface
{
    public function createMargherita()
    {
        return new Margherita("Venise");
    }

    public function createRegina()
    {
        return new Regina("Venise");
    }
}

class PizzeriaApplication
{
    public function createPizza(PizzaFactoryInterface $factory)
    {
        $margherita = $factory->createMargherita();
        $margherita->flip();
        $margherita->cook();
        $margherita->eat();

        $regina = $factory->createRegina();
        $regina->flip();
        $regina->cook();
        $regina->eat();
    }
}

$app = new PizzeriaApplication();
$app->createPizza(new NeapolitanPizzaFactory()); // Margherita et Regina napolitaine
$app->createPizza(new RomanPizzaFactory()); // Margherita et Regina romaine
$app->createPizza(new VenetianPizzaFactory()); // Margherita et Regina venizienne

// > Faire en sorte que "createPizza" prenne en paramètre une instance d'une factory qui respecte le contrat PizzaFactoryInterface
// > Depuis la factory passée en paramètre, créez une Margherita et appelez ses méthodes.
// > Toujours depuis la factory, créez une Regina et appelez ses méthodes.


// Résultat attendu :
/*
Un salto di margherita napoletana!
Croccante napoletana margherita!
Sei bella come una margherita napoletana!

Un salto di regina napoletana!
Croccante napoletana regina!
Sei bella come una regina napoletana!

--
Un salto di margherita romana!
Croccante romana margherita!
Sei bella come una margherita romana!

Un salto di regina romana!
Croccante romana regina!
Sei bella come una regina romana!

--
Un salto di margherita veneziana!
Croccante veneziana margherita!
Sei bella come una margherita veneziana!

Un salto di regina veneziana!
Croccante veneziana regina!
Sei bella come una regina veneziana!
*/
