<?php

// Singleton

namespace App;

use Exception;
use PDO;
use PDOException;

abstract class Database
{
    // Database $instance;
    private static $instance;

    private static $config = [
        'username' => 'admin',
        'password' => 'admin',
    ];

    public function __construct()
    {
    }

    // function getInstance(): Database
    public static function getPDO(): PDO
    {
        if (self::$instance === null) {
            try {
                self::$instance = new PDO("host;dbname;", self::$config['username'], self::$config['password']);
            } catch (PDOException $e) {
                throw new Exception("Connexion à la BDD impossible.");
            }
        }

        return self::$instance;
    }
}

/** à modifier */
$database1 = Database::getPDO(); // Créer l'instance
$database2 = Database::getPDO(); // La récupère seulement
$database3 = Database::getPDO();
$database4 = Database::getPDO();
$database5 = Database::getPDO();

Database::getPDO()->prepare('SELECT * FROM table_name;');

/**
 * Pas les mêmes instances :/
 *
 * Faire en sorte que $database1 et $database2 contiennent la même instance
 *
 * > HINT
 * - Allez chercher du côté du contexte """static"""
 * - Faites attention à la première récupération de l'instance...
 */
