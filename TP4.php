<?php

// Adapter

namespace App;

/**
 * "Plugin fourni par Strope"
 *
 * /!\ Ne pas toucher
 */
interface StropePaymentInterface
{
    public function amountInCents();
}

function sendPayment(StropePaymentInterface $payment)
{
    echo $payment->amountInCents();
}

/**
 * Début de votre code
 */
class Payment
{
    private $amount;

    public function __construct($amount)
    {
        $this->amount = $amount;
    }

    public function amountInEuros() {
        return $this->amount;
    }
}

class PaymentAdapter implements StropePaymentInterface
{
    private Payment $payment;

    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    public function amountInCents()
    {
        return $this->payment->amountInEuros() * 100;
    }
}

$payment = new PaymentAdapter(new Payment(10));
echo sendPayment($payment); // Erreur, la classe Payment ne peut pas être passée en paramètres de la fonction sendPayment()
echo PHP_EOL;

/**
 * Créer un Adapter permettant de convertir des euros en centimes pour les passer en paramètre de sendPayment()
 */
