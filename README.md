# php-design-pattern

| TP | Nom |
|---|---|
|TP1|Factory Method|
|TP2|Abstract Factory|
|TP3|Singleton|
|TP4|Adapter|
|TP5|Builder|
|TP6|Chain of responsibility|
|TP7?|Facade|
